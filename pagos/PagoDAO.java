/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sparkCarrito;
/**
 *
 * @author franco
 */
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.sql2o.Connection;
import java.util.List;
import lombok.Data; 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static sparkCarrito.CarritoControlador.registraLog;
import util.Sql2oDAO;

@Data
public class PagoDAO {
    
    
    public void insert(Pago p) {
        String insertSQL = "INSERT INTO PAGO (cuilCliente,nombre) VALUES (:cuil, :nombre,:direccion,:telefono,:email)";
        System.out.println(insertSQL);
        //try (Connection con = Conexion.getConexion()) {
        try (Connection con = Sql2oDAO.getSql2o().open()) {
             con.createQuery(insertSQL).bind(p).executeUpdate();
        } catch(Exception e){
            registraLog.error("Error al Insertar con {}", insertSQL, e);
        }
        registraLog.info("FINALIZO LA INSERCION {}", insertSQL);
    } 
     public void update(Pago p) {
        String updateSQL = "UPDATE PAGO SET   TIPO_PAGO = :Tipo_pago WHERE NROPAGO = :nroPago";
        System.out.println(updateSQL);
        try (Connection con = Sql2oDAO.getSql2o().open()) {
          con.createQuery(updateSQL).bind(p).executeUpdate();
        } catch(Exception e){
            registraLog.error("Error Update con {}", updateSQL, e);
        }
    }
    
}
