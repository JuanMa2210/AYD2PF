/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.util.HashMap;
import java.util.List;
import spark.Request;
import spark.Response;
import spark.Route;
import sparkCarrito.Pago;
import sparkCarrito.PagoDAO;
import util.Path;
import util.ViewUtil;

/**
 *
 * @author franco
 */
public class PagoControlador {
    public static Route pagarCompra = (Request request, Response response) -> {
      HashMap model = new HashMap();
      Pago p = new Pago();
      p.setNombre(request.queryParams("nombre")); 
      p.setEmail(request.queryParams("email"));
      p.setTelefono(Integer.parseInt(request.queryParams("telefono")));
      p.setCuil(Integer.parseInt(request.queryParams("cuil")));
      PagoDAO pDAO = new PagoDAO();
      pDAO.insert(p);
      
      // Inicializar formulario
      
     
      model.put("pagos", p);
      return ViewUtil.render(request, model, Path.Template.PAGOS);
      };
    
  public static Route updateCompra = (Request request, Response response) -> {
      HashMap model = new HashMap();
      
      Pago p = new Pago();
      p.setNroPago(Integer.valueOf(request.queryParams("nropago")));
      p.setTipo_pago(request.queryParams("formaPago"));
      
      PagoDAO pDAO = new PagoDAO();
      pDAO.update(p);
                          
      // Inicializar formulario
      model.put("pago", p);
      return ViewUtil.render(request, model, Path.Template.PAGOS);
    };
}
